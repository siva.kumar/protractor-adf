var Page = require("astrolabe").Page;
//var TestConfig = require("../../../test.config.js");
var Util = require('../../util/utils');
var loginpage = require('../loginpage');
module.exports = Page.create({

	LoginSuccess: {
		value: function() {
			this.waitForDivElements();
			this.waitUntilTheUserIsLogged();
			Util.waitForPage();
		}
	},

	waitUntilTheUserIsLogged: {
		value: function() {
			var landingLoginPage = element(by.className('alf-profile-name'));
			Util.waitUntilElementIsVisible(landingLoginPage);
		}
	},

	userinfoname: {
		get: function() {
			return element(by.xpath('/html/body/alfresco-app/div[2]/appsmanager-user-profile/div/div/span[1]'));
		}
	},

	waitForDivElements: {
		value: function() {
			Util.waitUntilElementIsVisible(element(by.className("alf-profile-container")));
		}
	},

	homePage: {
		value: function() {
			var EC = protractor.ExpectedConditions;
			var homepage = element(by.xpath('/html/body/alfresco-app/div[3]/appsmanager-breadcrumbs/div[1]/div[1]/div'));
			browser.wait(EC.visibilityOf(homepage), 2000);
			homepage.click();

		}
	},

	appmangerClick: {
		value: function() {
			var EC = protractor.ExpectedConditions;
			var expand = element(by.xpath('/html/body/alfresco-app/div[1]/div/i'));
			browser.wait(EC.visibilityOf(expand), 5000);
			expand.click();

			var expandadmin = element(by.xpath('/html/body/alfresco-app/div[1]/alfresco-side-menu/div/div/div/div[3]/i'));
			browser.wait(EC.visibilityOf(expandadmin), 5000);
			expandadmin.click();

			var app = element(by.xpath('/html/body/alfresco-app/div[1]/alfresco-side-menu/div/div/div[2]/div'));
			browser.wait(EC.visibilityOf(app), 5000);
			app.click();

			browser.wait(EC.visibilityOf(element(by.xpath('/html/body/alfresco-app/div[3]/appsmanager-breadcrumbs/div[1]/div[2]/div'))), 5000);

		}
	},

	expenseClick: {
		value: function() {
			var EC = protractor.ExpectedConditions;
			var expenseApp = element(by.xpath('/html/body/alfresco-app/div[3]/main/div/appsmanager-apps/appsmanager-appsview/div/div/div[1]/div[2]/h1'));
			browser.wait(EC.visibilityOf(expenseApp), 2000);
			expenseApp.click();
		}
	},

	logOut: {
		value: function() {
			var EC = protractor.ExpectedConditions;
			var userLogo = element(by.xpath('//*[@id="alf-profile-pic"]'));
			browser.wait(EC.visibilityOf(userLogo), 500000);
			userLogo.click();
			var logout = element(by.xpath('/html/body/alfresco-app/div[2]/appsmanager-user-profile/div/div/div[1]/ul/li[1]'));
			browser.wait(EC.visibilityOf(logout), 50000);
			logout.click();

		}
	}
});