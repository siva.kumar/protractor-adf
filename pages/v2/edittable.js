var Page = require("astrolabe").Page;
var Util = require('../../util/utils');
var TestConfig = require("../../test.config");
var appMappingPage = require('./appsMappingPage');
var EC = protractor.ExpectedConditions;
module.exports = Page.create({

	tablesavebutton: {
		get: function() {
			return element(by.xpath(
				"html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div[2]/appsmanager-appsmappinglist/div/div[2]/form/div[3]/button[1]"
			));
		}
	},
	tablediscardbutton: {
		get: function() {
			return element(by.xpath(
				"/html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div[2]/appsmanager-appsmappinglist/div/div[2]/form/div[3]/button[2]"
			));
		}
	},
  
	clickOnTableSaveButton: {
		value: function() {
			browser.wait(EC.visibilityOf(element(by.xpath(
				"html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div[2]/appsmanager-appsmappinglist/div/div[2]/form/div[3]/button[1]"
			))), 5000);
			Util.waitUntilElementIsVisible(element(by.xpath(
				"html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div[2]/appsmanager-appsmappinglist/div/div[2]/form/div[3]/button[1]"
			)));
			this.tablesavebutton.click();
			Util.waitForPage();
			browser.sleep(1000);
			this.alertOk();
			Util.waitForPage();
		}
	},
  
	alertOk: {
		value: function() {
			browser.sleep(1000);
			var alertOk = element(by.xpath(
				"html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div[2]/appsmanager-appsmappinglist/appsmanager-alertbox/dialog/div[3]/button"
			));
			browser.wait(EC.visibilityOf(alertOk), 1000);
			alertOk.click();
			Util.waitForPage();
		}
	},
  
	alertConfirm: {
		value: function() {
			browser.sleep(1000);
			var alertConfirm = element(by.xpath(".//*[@id='apps-manager-details-confirmbox']/div[3]/button[1]"));
			browser.wait(EC.visibilityOf(alertConfirm), 1000);
			alertConfirm.click();
		}
	},
  
	alertCancle: {
		value: function() {
			browser.sleep(1000);
			var alertCancle = element(by.xpath(".//*[@id='apps-manager-details-confirmbox']/div[3]/button[2]"));
			browser.wait(EC.visibilityOf(alertCancle), 1000);
			alertCancle.click();
		}
	},
  
	user_group_row: {
		get: function() {
			return element(by.xpath(
				"html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div[2]/appsmanager-appsmappinglist/div/div[2]/form/div[2]/table/tbody/tr[1]/td[1]"
			));
		}
	},
  
	adf_page_row: {
		get: function() {
			return element(by.xpath(
				"html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div[2]/appsmanager-appsmappinglist/div/div[2]/form/div[2]/table/tbody/tr[1]/td[2]"
			));
		}
	},
  
	adf_url_row: {
		get: function() {
			return element(by.xpath(
				"html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div[2]/appsmanager-appsmappinglist/div/div[2]/form/div[2]/table/tbody/tr[1]/td[3]"
			));
		}
	},
  
	edit_row: {
		get: function() {
			return element(by.xpath(
				"html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div[2]/appsmanager-appsmappinglist/div/div[2]/form/div[2]/table/tbody/tr[1]/td[4]/i[1]"
			));
		}
	},
  
	delet_mapping: {
		get: function() {
			return element(by.xpath(
				"html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div[2]/appsmanager-appsmappinglist/div/div[2]/form/div[2]/table/tbody/tr[1]/td[4]/i[2]"
			));
		}
	},
  
	click_On_delet_mapping: {
		value: function() {
			Util.waitUntilElementIsVisible(this.delet_mapping);
			this.delet_mapping.click();
		}
	},
  
	click_On_edit_row: {
		value: function() {
			Util.waitUntilElementIsVisible(this.edit_row);
			this.edit_row.click();
		}
	},
  
	waitForElements: {
		value: function() {
			Util.waitUntilElementIsVisible(this.user_group_row);
			Util.waitUntilElementIsVisible(this.delet_mapping);
			Util.waitUntilElementIsVisible(this.edit_row);
		}
	},
  
	clickon_editmappingButton: {
		value: function() {
			this.waitForElements();
			Util.waitUntilElementIsVisible(this.edit_row);
			this.click_On_edit_row();
		}
	},
  
	clickon_deletemappingButton: {
		value: function() {
			this.waitForElements();
			this.click_On_delet_mapping();
			browser.wait(EC.visibilityOf(this.tablesavebutton, 2000));
			Util.waitForPage();
			this.clickOnTableSaveButton();
		}
	},
});