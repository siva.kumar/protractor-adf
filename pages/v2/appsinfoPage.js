var Page = require("astrolabe").Page;
var Util = require('../../util/utils');
var TestConfig = require("../../test.config");
module.exports = Page.create({
	parentDiv: {
		get: function() {
			return element(by.id('appinfo_container'));
		}
	},
	childDiv: {
		get: function() {
			return element(by.className('bar alf-grpbx-header'));
		}
	},
	waitForElement: {
		value: function(element) {
			Util.waitUntilElementIsVisible(element);
			//return element(by.className('bar alf-grpbx-header'));
		}
	},
	appId: {
		get: function() {
			return element(by.id('appinfo_appId'));
		}
	},
	appModelId: {
		get: function() {
			return element(by.id('appinfo_appModelId'));
		}
	},
	appDeployedId: {
		get: function() {
			return element(by.id('appinfo_appDeployedId'));
		}
	},
	appName: {
		get: function() {
			return element(by.id('appInfo_appName'));
		}
	},
	appDescription: {
		get: function() {
			return element(by.id('appInfo_appDescription'));
		}
	},
	waitForAppInfoElements: {
		value: function() {
			Util.waitUntilElementIsVisible(this.parentDiv);
			Util.waitUntilElementIsVisible(this.childDiv);
			Util.waitUntilElementIsVisible(this.appName);
			Util.waitUntilElementIsVisible(this.appId);
		}
	},
});