var Page = require("astrolabe").Page;
var Util = require('../../util/utils');
var appsManager = require('./appsManagerPage');
var appInfoPage = require('./appsinfoPage');
var appsMappingPage = require('./appsMappingPage');
var TestConfig = require("../../test.config");
var edittable = require('./edittable');
var loginPage = require('../loginpage');
var sideBar = require('./sidebarpage');
var dashBoardPage = require('./dashboardPage');
var navbar = require('../../pages/common/navbar');
var EC = protractor.ExpectedConditions;
module.exports = Page.create({
	clickOnExpenseApp: {
		value: function() {
			sideBar.clickOnApp('html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div/appsmanager-appslist/div/div[2]/div[2]/div[1]/div[2]');
			appsManager.waitForAppsManagerElements();
			appInfoPage.waitForAppInfoElements();
			Util.waitForPage();
		}
	},
	clickOnClaimApp: {
		value: function() {
			Util.waitUntilElementIsVisible(element(by.xpath('html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div/appsmanager-appslist/div/div[2]/div[2]/div[2]/div[2]')));
			sideBar.clickOnApp("html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div/appsmanager-appslist/div/div[2]/div[2]/div[2]/div[2]");
			appsManager.waitForAppsManagerElements();
			appInfoPage.waitForAppInfoElements();
			Util.waitForPage();
		}
	},
	addAppMapping: {
		value: function(role, AdfPageName, AdfUrl) {
			appsMappingPage.addApps(role, AdfPageName, AdfUrl);
			Util.waitForPage();
		}
	},
	clickOnTravelApp: {
		value: function() {
			sideBar.clickOnApp('html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div/appsmanager-appslist/div/div[2]/div[2]/div[4]/div[2]');
			appsManager.waitForAppsManagerElements();
			appInfoPage.waitForAppInfoElements();
			Util.waitForPage();
		}
	},
	clickOnLeaveApp: {
		value: function() {
			sideBar.clickOnApp('html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div/appsmanager-appslist/div/div[2]/div[2]/div[3]/div[2]');
			appsManager.waitForAppsManagerElements();
			appInfoPage.waitForAppInfoElements();
			Util.waitForPage();
		}
	},
	clickOnEdit: {
		value: function(option, adfPageName, adfpageurl) {
			edittable.waitForElements();
			edittable.clickon_editmappingButton();
			appsMappingPage.editAppsInfo(option, adfPageName, adfpageurl);
		}
	},
	clickOnDelete: {
		value: function(option, adfPageName, adfpageurl) {
			edittable.waitForElements();
			edittable.clickon_deletemappingButton();
		}
	},
	activitMenu: {
		value: function() {
			dashBoardPage.clickOnActivitiMenu();
			Util.waitForPage();
		}
	},
	dashBoardActiviti: {
		value: function() {
			this.activitMenu();
			dashBoardPage.clickonDashBoard();
			Util.waitForPage();
		}
	},
	tasksActiviti: {
		value: function() {
			this.activitMenu();
			dashBoardPage.clickOnTasksMenu();
			dashBoardPage.completedTaskOption.click();
			expect(element(by.xpath(".//*[@id='tasks']/div/div/div/activiti-tasklist/div/div/alfresco-datatable/table/thead/tr/th[1]")).isPresent()).toBeTruthy();
			expect(element(by.xpath(".//*[@id='tasks']/div/div/div/activiti-tasklist/div/div/alfresco-datatable/table/thead/tr/th[2]/span")).isPresent()).toBeTruthy();
			this.activitMenu();
			dashBoardPage.involedTaskOption.click();
			expect(element(by.xpath(".//*[@id='tasks']/div/div/div/activiti-tasklist/div/div/alfresco-datatable/table/tbody/tr[1]/td[1]")).isPresent()).toBeTruthy();
			expect(element(by.xpath("html/body/alfresco-app/div[3]/appsmanager-breadcrumbs/div[1]/div[4]/div")).isPresent()).toBeTruthy();
			this.activitMenu();
			dashBoardPage.queuedTaskOption.click();
			this.activitMenu();
			dashBoardPage.myTaskOption.click();
			expect(element(by.xpath(".//*[@id='tasks']/div/div/div/activiti-tasklist/div/div/alfresco-datatable/table/tbody/tr[1]/td[1]")).isPresent()).toBeTruthy();
			expect(element(by.xpath("html/body/alfresco-app/div[3]/appsmanager-breadcrumbs/div[1]/div[4]/div")).isPresent()).toBeTruthy();
			Util.waitForPage();
		}
	},
	processActivit: {
		value: function() {
			this.activitMenu();
			dashBoardPage.clickOnProcessMenu();
			dashBoardPage.runningProcessOption.click();
			expect(element(by.xpath(".//*[@id='processes']/div/div/div/div[1]/div[1]")).isPresent()).toBeTruthy();
			expect(element(by.xpath(".//*[@id='processes']/div/div/div/div[2]/div")).isPresent()).toBeTruthy();;
			this.activitMenu();
			dashBoardPage.completedProcessOption.click();
			expect(element(by.xpath(".//*[@id='processes']/div/div/div/div[1]/div[1]")).isPresent()).toBeTruthy();
			expect(element(by.xpath(".//*[@id='processes']/div/div/div/div[2]/div")).isPresent()).toBeTruthy();
			this.activitMenu();
			dashBoardPage.AllProcessOption.click();
			expect(element(by.xpath(".//*[@id='processes']/div/div/div/div[1]/div[1]")).isPresent()).toBeTruthy();
			expect(element(by.xpath(".//*[@id='processes']/div/div/div/div[2]/div")).isPresent()).toBeTruthy();
			Util.waitForPage();
		}
	},
	reportActiviti: {
		value: function() {
			this.activitMenu();
			dashBoardPage.clickOnReportsMenu();
			dashBoardPage.myTaskReportOption.click();
			expect(element(by.xpath("html/body/alfresco-app/div[3]/appsmanager-breadcrumbs/div[1]/div[4]/div")).isPresent()).toBeTruthy();
			this.activitMenu();
			dashBoardPage.processReportOption.click();
			expect(element(by.xpath("html/body/alfresco-app/div[3]/appsmanager-breadcrumbs/div[1]/div[4]/div")).isPresent()).toBeTruthy();
			this.activitMenu();
			dashBoardPage.taskServiceReportOption.click();
			expect(element(by.xpath("html/body/alfresco-app/div[3]/appsmanager-breadcrumbs/div[1]/div[4]/div")).isPresent()).toBeTruthy();
			Util.waitForPage();
		}
	},
	adminActiviti: {
		value: function() {
			dashBoardPage.clickOnAdminMenu();
			browser.sleep(2000);
			dashBoardPage.clickOnAppsManager();
			Util.waitForPage();
		}
	},
	logOut: {
		value: function() {
			navbar.homePage();
			Util.waitForPage();
			navbar.expenseClick();
			navbar.logOut();
			Util.waitForPage();
			browser.sleep(5000);
		}
	}
});