var Page = require('astrolabe').Page;
var Util = require('../../util/utils');
var navbar = require('../../pages/common/navbar');
var TestConfig = require("../../test.config");
module.exports = Page.create({
	activitiMenu: {
		get: function() {
			return element(by.xpath('html/body/alfresco-app/div[1]/div/i'));
		}
	},
	waitForElements: {
		value: function() {
			Util.waitUntilElementIsVisible(this.activitiMenu);
		}
	},
	clickOnActivitiMenu: {
		value: function() {
			this.waitForElements();
			this.activitiMenu.click();
		}
	},
	dashBoardButton: {
		get: function() {
			return element(by.xpath('html/body/alfresco-app/div[1]/alfresco-side-menu/div[2]/div[1]/div/div[2]'));
		}
	},
	clickonDashBoard: {
		value: function() {
			Util.waitUntilElementIsVisible(this.dashBoardButton);
			this.dashBoardButton.click();
		}
	},
	tasks: {
		get: function() {
			return element(by.xpath('html/body/alfresco-app/div[1]/alfresco-side-menu/div[2]/div[2]/div[1]/div[2]'));
		}
	},
	tasksMenu: {
		get: function() {
			return element(by.xpath('html/body/alfresco-app/div[1]/alfresco-side-menu/div[2]/div[2]/div[1]/div[3]/i'));
		}
	},
	clickOnTasksMenu: {
		value: function() {
			Util.waitUntilElementIsVisible(this.tasksMenu);
			this.tasksMenu.click();
		}
	},
	completedTaskOption: {
		get: function() {
			Util.waitForPage();
			return element(by.xpath('html/body/alfresco-app/div[1]/alfresco-side-menu/div[2]/div[2]/div[2]/div[1]'));
		}
	},
	involedTaskOption: {
		get: function() {
			Util.waitForPage();
			return element(by.xpath('html/body/alfresco-app/div[1]/alfresco-side-menu/div[2]/div[2]/div[2]/div[2]'));
		}
	},
	queuedTaskOption: {
		get: function() {
			Util.waitForPage();
			return element(by.xpath('html/body/alfresco-app/div[1]/alfresco-side-menu/div[2]/div[2]/div[2]/div[3]'));
		}
	},
	myTaskOption: {
		get: function() {
			Util.waitForPage();
			return element(by.xpath('html/body/alfresco-app/div[1]/alfresco-side-menu/div[2]/div[2]/div[2]/div[4]'));
		}
	},
	process: {
		get: function() {
			Util.waitForPage();
			return element(by.xpath('html/body/alfresco-app/div[1]/alfresco-side-menu/div[2]/div[3]/div/div[2]'));
		}
	},
	processMenu: {
		get: function() {
			Util.waitForPage();
			return element(by.xpath('html/body/alfresco-app/div[1]/alfresco-side-menu/div[2]/div[3]/div/div[3]/i'));
		}
	},
	clickOnProcessMenu: {
		value: function() {
			Util.waitUntilElementIsVisible(this.processMenu);
			this.processMenu.click();
			Util.waitForPage();
		}
	},
	runningProcessOption: {
		get: function() {
			Util.waitForPage();
			return element(by.xpath('html/body/alfresco-app/div[1]/alfresco-side-menu/div[2]/div[3]/div[2]/div[1]'));
		}
	},
	completedProcessOption: {
		get: function() {
			Util.waitForPage();
			return element(by.xpath('html/body/alfresco-app/div[1]/alfresco-side-menu/div[2]/div[3]/div[2]/div[2]'));
		}
	},
	AllProcessOption: {
		get: function() {
			Util.waitForPage();
			return element(by.xpath('html/body/alfresco-app/div[1]/alfresco-side-menu/div[2]/div[3]/div[2]/div[3]'));
		}
	},
	reports: {
		get: function() {
			Util.waitForPage();
			return element(by.xpath('html/body/alfresco-app/div[1]/alfresco-side-menu/div[2]/div[4]/div/div[2]'));
		}
	},
	reportsMenu: {
		get: function() {
			Util.waitForPage();
			return element(by.xpath('html/body/alfresco-app/div[1]/alfresco-side-menu/div[2]/div[4]/div/div[3]/i'));
		}
	},
	clickOnReportsMenu: {
		value: function() {
			Util.waitUntilElementIsVisible(this.reportsMenu);
			this.reportsMenu.click();
			Util.waitForPage();
		}
	},
	myTaskReportOption: {
		get: function() {
			Util.waitForPage();
			return element(by.xpath('html/body/alfresco-app/div[1]/alfresco-side-menu/div[2]/div[4]/div[2]/div[1]'));
		}
	},
	processReportOption: {
		get: function() {
			Util.waitForPage();
			return element(by.xpath('html/body/alfresco-app/div[1]/alfresco-side-menu/div[2]/div[4]/div[2]/div[2]'));
		}
	},
	taskServiceReportOption: {
		get: function() {
			Util.waitForPage();
			return element(by.xpath('html/body/alfresco-app/div[1]/alfresco-side-menu/div[2]/div[4]/div[2]/div[3]'));
		}
	},
	adminMenu: {
		get: function() {
			Util.waitForPage();
			return element(by.xpath('html/body/alfresco-app/div[1]/alfresco-side-menu/div[3]/div/div[1]/div[3]/i'));
		}
	},
	clickOnAdminMenu: {
		value: function() {
			Util.waitUntilElementIsVisible(this.adminMenu);
			this.adminMenu.click();
			Util.waitForPage();
		}
	},
	appsManager: {
		get: function() {
			Util.waitForPage();
			return element(by.xpath('html/body/alfresco-app/div[1]/alfresco-side-menu/div[3]/div/div[2]/div'));
		}
	},
	clickOnAppsManager: {
		value: function() {
			Util.waitUntilElementIsVisible(this.appsManager);
			this.appsManager.click();
		}
	},
});