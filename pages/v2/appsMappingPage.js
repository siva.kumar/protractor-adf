var Page = require("astrolabe").Page;
var Util = require('../../util/utils');
var TestConfig = require("../../test.config");
var edittable = require('./edittable');
var EC = protractor.ExpectedConditions;
module.exports = Page.create({
	url: {
		value: TestConfig.admin.appsManagerContextRoot
	},
	get: {
		value: function() {
			browser.get(this.url);
		}
	},
	render: {
		value: function() {
			Util.waitForPage();
		}
	},
	tableHeader: {
		get: function() {
			return element(by.css('.bar.alf-grpbx-header'));
		}
	},
	addbutton: {
		get: function() {
			return element(by.className('alf-tbl-button mdl-shadow--2dp'));
		}
	},
	waitForElement: {
		value: function(element) {
			Util.waitUntilElementIsVisible(this.addbutton);
		}
	},
	clickonAddButton: {
		value: function() {
			Util.waitUntilElementIsVisible(this.addbutton);
			this.addbutton.click();
		}
	},

	defaultadfpage: {
		get: function() {
			return element(by.xpath(
				'/html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div[2]/appsmanager-appsmappinglist/div/div[2]/form/div[1]/input'
			));
		}
	},
	enterdefaultadfpage: {
		value: function(pagename) {
			Util.waitUntilElementIsVisible(this.defaultadfpage);
			this.defaultadfpage.clear();
			this.defaultadfpage.sendKeys(pagename);
		}
	},

	cleardefaultadfpage: {
		value: function() {
			Util.waitUntilElementIsVisible(this.defaultadfpage);
			this.defaultadfpage.clear();
		}
	},
	adfPageName: {
		get: function() {
			return element(by.xpath(".//*[@id='txt-appsmapping-adfpagename']"));
		}
	},
	enteradfPageName: {
		value: function(adfPageName) {
			Util.waitUntilElementIsVisible(this.adfPageName);
			this.adfPageName.clear();
			this.adfPageName.sendKeys(adfPageName);
		}
	},
	adfpageurl: {
		get: function() {
			return element(by.xpath(".//*[@id='apps-manager-details-dialog']/form/div[2]/div[3]/input"));
		}
	},
	enteradfpageurl: {
		value: function(adfpageurl) {
			Util.waitUntilElementIsVisible(this.adfpageurl);
			this.adfpageurl.clear();
			this.adfpageurl.sendKeys(adfpageurl);
		}
	},
	selectBox: {
		get: function() {
			return element(by.xpath("//*[@id='apps-manager-details-dialog']/form/div[2]/div[1]/select"));
		}
	},
	selectOption: {
		value: function(optionText) {
			Util.waitUntilElementIsVisible(this.selectBox);
			var selOption = element(by.cssContainingText("option", optionText));
			this.selectBox.click();
			Util.waitUntilElementIsVisible(selOption);
			selOption.click();
			this.selectBox.sendKeys(protractor.Key.chord(protractor.Key.ENTER));
			selOption.click();
		}
	},
	savebutton: {
		get: function() {
			return element(by.xpath(".//*[@id='apps-manager-details-dialog']/form/div[3]/button[1]"));
		}
	},
	discardbutton: {
		get: function() {
			return element(by.xpath(".//*[@id='apps-manager-details-dialog']/form/div[3]/button[2]"));
		}
	},
	clickSaveButton: {
		value: function() {
			Util.waitUntilElementIsVisible(this.savebutton);
			this.savebutton.click();
		}
	},
	clickDiscardButton: {
		value: function() {
			Util.waitUntilElementIsVisible(this.discardbutton);
			this.discardbutton.click();
		}
	},
	waitForAppMappingElements: {
		value: function() {
			Util.waitUntilElementIsVisible(this.adfpageurl);
			Util.waitUntilElementIsVisible(this.adfPageName);
		}
	},
	addApps: {
		value: function(option, adfPageName, adfpageurl) {
			Util.waitUntilElementIsVisible(this.addbutton);
			this.clickonAddButton();
			browser.switchTo();
			this.waitForAppMappingElements();
			Util.waitUntilElementIsVisible(this.selectBox);
			browser.sleep(1000);
			this.selectOption(option);
			browser.sleep(1000);
			browser.wait(EC.visibilityOf(this.savebutton), 1000);
			Util.waitUntilElementIsVisible(this.adfPageName);
			this.enteradfpageurl(adfpageurl);
			Util.waitUntilElementIsVisible(this.adfpageurl);
			this.enteradfPageName(adfPageName);
			browser.sleep(1000);
			Util.waitUntilElementIsVisible(this.savebutton);
			this.clickSaveButton();
			Util.waitForPage();
			browser.wait(EC.visibilityOf(element(by.xpath("/html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div[2]/appsmanager-appsmappinglist/div/div[2]/form/div[3]/button[1]"))), 5000);
			Util.waitUntilElementIsVisible(element(by.xpath("/html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div[2]/appsmanager-appsmappinglist/div/div[2]/form/div[3]/button[1]")));
			edittable.clickOnTableSaveButton();
			Util.waitForPage();
		}
	},
	editAppsInfo: {
		value: function(option, adfPageName, adfpageurl) {
			browser.switchTo();
			this.waitForAppMappingElements();
			Util.waitUntilElementIsVisible(this.selectBox);
			browser.sleep(1000);
			this.selectOption(option);
			browser.sleep(1000);
			browser.wait(EC.visibilityOf(this.savebutton), 1000);
			Util.waitUntilElementIsVisible(this.adfPageName);
			this.enteradfpageurl(adfpageurl);
			Util.waitUntilElementIsVisible(this.adfpageurl);
			this.enteradfPageName(adfPageName);
			browser.sleep(1000);
			Util.waitUntilElementIsVisible(this.savebutton);
			this.clickSaveButton();
			Util.waitForPage();
			browser.wait(EC.visibilityOf(element(by.xpath("/html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div[2]/appsmanager-appsmappinglist/div/div[2]/form/div[3]/button[1]"))), 5000);
			Util.waitUntilElementIsVisible(element(by.xpath("/html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div[2]/appsmanager-appsmappinglist/div/div[2]/form/div[3]/button[1]")));
			edittable.clickOnTableSaveButton();
			Util.waitForPage();
		}
	},

	errordefaultadfpage: {
		get: function() {
			return element(by.xpath(
				'/html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div[2]/appsmanager-appsmappinglist/div/div[2]/form/div[1]/div'
			));
		}
	},
	waitforerrordefaultpage: {
		value: function() {
			Util.waitUntilElementIsVisible(this.errordefaultadfpage);
		}
	},

	erroradfPageName: {
		value: function() {
			Util.waitUntilElementIsVisible(element(by.xpath('//*[@id="apps-manager-details-dialog"]/form/div[2]/div[2]/div')));
		}
	},

	geterroradfPageName: {
		get: function() {
			return element(by.xpath('//*[@id="apps-manager-details-dialog"]/form/div[2]/div[2]/div'));
		}
	},
	geterrorselectBox: { get: function() {
     return element(by.xpath('//*[@id="apps-manager-details-dialog"]/form/div[2]/div[1]/div'));
	}},

	errorselectBox: {
		value: function() {
			Util.waitUntilElementIsVisible(element(by.xpath('//*[@id="apps-manager-details-dialog"]/form/div[2]/div[1]/div')));
		}
	},

	erroradfpageurl: {
		value: function() {
			Util.waitUntilElementIsVisible(element(by.xpath('//*[@id="apps-manager-details-dialog"]/form/div[2]/div[3]/div')));
		}
	},

	geterroradfPageUrl: {
		get: function() {
			return element(by.xpath('//*[@id="apps-manager-details-dialog"]/form/div[2]/div[3]/div'));
		}
	},
	validateForm: {
		value: function() {
			
			Util.waitUntilElementIsVisible(this.defaultadfpage);
			this.cleardefaultadfpage();
	      	this.enterdefaultadfpage('1');
	      	this.waitforerrordefaultpage();
			expect(this.errordefaultadfpage.getText()).toEqual('Default directory name should contain 3 characters, Default directory name should start with Alphabet');
			expect(edittable.clickOnTableSaveButton().isEnabled()).toBe(false);

			Util.waitUntilElementIsVisible(this.addbutton);
			this.clickonAddButton();
			browser.switchTo();
			this.waitForAppMappingElements();
			Util.waitUntilElementIsVisible(this.selectBox);
			browser.sleep(1000);
			this.selectOption('Manager');
			this.selectOption('--Select--');
			this.errorselectBox();
			expect(this.geterrorselectBox.getText()).toEqual('Make a proper selection');
			browser.sleep(1000);
	
			Util.waitUntilElementIsVisible(this.adfPageName);
			this.enteradfPageName('1');
			this.erroradfPageName();
	      	expect(this.geterroradfPageName.getText()).toEqual('Page name should start with alphabet, Page name should contain atleast 3 characters');
			browser.sleep(1000);
			
			Util.waitUntilElementIsVisible(this.adfpageurl);
			this.enteradfpageurl('test');
			this.erroradfpageurl();
	      	expect(this.geterroradfPageUrl.getText()).toEqual('Page URL should contain webpage extension(html/htm/php/jsp)');
	      	expect(this.savebutton.isEnabled()).toBe(false);
	      	this.clickDiscardButton();			
			Util.waitForPage();
		}
	},

defaultAdfPageName: {
		value: function(defaultAdfPageName) {
			Util.waitUntilElementIsVisible(this.defaultadfpage);
			this.cleardefaultadfpage();
	      	this.enterdefaultadfpage(defaultAdfPageName);
		}
	}



});
