var Page = require('astrolabe').Page;
var Util = require('../../util/utils');
var TestConfig = require("../../test.config");
var appsinfo = require("./appsinfoPage");
var appsDetails = {};
module.exports = Page.create({
	sideBar: {
		get: function() {
			return element(by.xpath('html/body/alfresco-app/div[1]/div/i'));
		}
	},
	adminpage: {
		get: function() {
			return element(by.xpath('html/body/alfresco-app/div[1]/alfresco-side-menu/div/div/div/div[3]/i'));
		}
	},
	appsManagerButton: {
		get: function() {
			return element(by.xpath('html/body/alfresco-app/div[1]/alfresco-side-menu/div/div/div[2]/div'));
		}
	},
	waitForElement: {
		value: function() {
			Util.waitUntilElementIsVisible(this.sideBar);
		}
	},
	clickonSideBar: {
		value: function() {
			this.waitForElement();
			this.sideBar.click();
		}
	},
	clickonAdminPage: {
		value: function() {
			Util.waitUntilElementIsVisible(element(by.xpath('html/body/alfresco-app/div[1]/alfresco-side-menu/div/div/div/div[3]/i')));
			this.adminpage.click();
		}
	},
	click_on_appsManagerButton: {
		value: function() {
			Util.waitUntilElementIsVisible(element(by.xpath('html/body/alfresco-app/div[1]/alfresco-side-menu/div/div/div[2]/div')));
			this.appsManagerButton.click();
		}
	},
	selectExpenseAppFromList: {
		get: function() {
			return element(by.xpath('html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div/appsmanager-appslist/div/div[2]/div[2]/div[1]/div[2]'));
		}
	},
	selectTravelAppFromList: {
		get: function() {
			return element(by.xpath('html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div/appsmanager-appslist/div/div[2]/div[2]/div[3]/div[2]'));
		}
	},
	clickOnApp: {
		value: function(selecteApp) {
			var selectedApp = element(by.xpath(selecteApp));
			Util.waitUntilElementIsVisible(selectedApp);
			selectedApp.click();
			Util.waitForPage();
			appsinfo.waitForAppInfoElements();
		}
	},
});