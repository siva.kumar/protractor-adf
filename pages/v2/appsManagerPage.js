var Page = require("astrolabe").Page;
var Util = require('../../util/utils');
var TestConfig = require("../../test.config");
var appsinfoPage = require("./appsinfoPage");
var navbar = require('../../pages/common/navbar');
var EC = protractor.ExpectedConditions;
var shareUtil = require("../../util/shareUtill");
var ur = "/alfresco/apps/#/appsmanager";
var AppDetails = [{
	name: '',
	id: 1,
	deploymentid: 111
}, {
	name: 'Expense Management',
	id: 4,
	deploymentid: 22501
}, {
	name: 'Claim Management App',
	id: 3,
	deploymentid: 22505
}, {
	name: 'Leave App',
	id: 1,
	deploymentid: 20005
}, {
	name: 'Travel Management',
	id: 5,
	deploymentid: 20001
}, {
	name: 'My Test App',
	id: 1001,
	deploymentid: 15013
}, {
	name: 'APInvoicePOCApp',
	id: 2002,
	deploymentid: 47501
}];
module.exports = Page.create({
	url: {
		value: TestConfig.admin.appsManagerContextRoot
	},
	get: {
		value: function() {
			browser.get(ur);
		}
	},
	nav: {
		get: function() {
			return navbar
		}
	},
	appInfo: {
		get: function() {
			return appsinfoPage;
		}
	},
	render: {
		value: function() {
			Util.waitForPage();
		}
	},
	waitForElements: {
		value: function() {
			Util.waitUntilElementIsVisible(element(by.xpath("html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div/appsmanager-appslist/div/div[1]")));
			Util.waitUntilElementIsVisible(element(by.xpath(".//*[@id='apps_list_search']")));
		}
	},
	clickOnApp: {
		value: function() {
			var selectAppFromAppList = element(by.xpath('html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div/appsmanager-appslist/div/div[2]/div[2]/div[1]/div[2]'));
			Util.waitUntilElementIsVisible(selectAppFromAppList);
			this.selectAppFromAppList.click();
			Util.waitForPage();
		}
	},
	selectAppRandomly: {
		value: function() {
			for (var i = 1; i <= 6; i++) {
				switch (i) {
					case 1:
						var selectAppFromAppList = element(by.xpath('html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div[1]/appsmanager-appslist/div/div[2]/div[2]/div[' + i + ']/div[2]'));
						Util.waitUntilElementIsVisible(selectAppFromAppList);
						selectAppFromAppList.click();
						//  Util.waitForPage();
						this.appInfo.waitForAppInfoElements();
						expect(this.appInfo.appName.getText()).toEqual(AppDetails[i].name);
						break;
					case 2:
						var selectAppFromAppList = element(by.xpath('html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div[1]/appsmanager-appslist/div/div[2]/div[2]/div[' + i + ']/div[2]'));
						Util.waitUntilElementIsVisible(selectAppFromAppList);
						selectAppFromAppList.click();
						//  Util.waitForPage();
						this.appInfo.waitForAppInfoElements();
						expect(this.appInfo.appName.getText()).toEqual(AppDetails[i].name);
						break;
					case 3:
						var selectAppFromAppList = element(by.xpath('html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div[1]/appsmanager-appslist/div/div[2]/div[2]/div[' + i + ']/div[2]'));
						Util.waitUntilElementIsVisible(selectAppFromAppList);
						selectAppFromAppList.click();
						//  Util.waitForPage();
						this.appInfo.waitForAppInfoElements();
						expect(this.appInfo.appName.getText()).toEqual(AppDetails[i].name);
						break;
					case 4:
						var selectAppFromAppList = element(by.xpath('html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div[1]/appsmanager-appslist/div/div[2]/div[2]/div[' + i + ']/div[2]'));
						Util.waitUntilElementIsVisible(selectAppFromAppList);
						selectAppFromAppList.click();
						//  Util.waitForPage();
						this.appInfo.waitForAppInfoElements();
						expect(this.appInfo.appName.getText()).toEqual(AppDetails[i].name);
						break;
					case 5:
						var selectAppFromAppList = element(by.xpath('html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div[1]/appsmanager-appslist/div/div[2]/div[2]/div[' + i + ']/div[2]'));
						Util.waitUntilElementIsVisible(selectAppFromAppList);
						selectAppFromAppList.click();
						//  Util.waitForPage();
						this.appInfo.waitForAppInfoElements();
						expect(this.appInfo.appName.getText()).toEqual(AppDetails[i].name);
						break;
					case 6:
						var selectAppFromAppList = element(by.xpath('html/body/alfresco-app/div[4]/main/div/apps-manager-layout-div/div[1]/appsmanager-appslist/div/div[2]/div[2]/div[' + i + ']/div[2]'));
						Util.waitUntilElementIsVisible(selectAppFromAppList);
						selectAppFromAppList.click();
						this.appInfo.waitForAppInfoElements();
						expect(this.appInfo.appName.getText()).toEqual(AppDetails[i].name);
						break;
				}
			};
		}
	},
	appDetailsHeader: {
		get: function() {
			return element(by.className('bar alf-grpbx-header'));
		}
	},
	waitForAppsManagerElements: {
		value: function() {
			Util.waitUntilElementIsVisible(this.appDetailsHeader);
		}
	},
});