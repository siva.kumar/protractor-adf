 var Page = require('astrolabe').Page;
 var Util = require('../util/utils');
 var navbar = require('../pages/common/navbar');
 var TestConfig = require("../test.config");
 //var url = 'http://localhost:9999/login';      
 module.exports = Page.create({
 	waitForDashboardIcon: {
 		value: function() {
 			Util.waitUntilElementIsVisible(element(by.xpath('/html/body/alfresco-app/div[1]/alfresco-side-menu/div[1]/div[1]/div/div/i')));
 		}
 	},
 	isSelectedDashboardIcon: {
 		value: function() {
 			expect(element(by.xpath('/html/body/alfresco-app/div[1]/alfresco-side-menu/div[1]/div[1]/div/div/i')).isEnabled).toBe(true);
 		}
 	},
 	waitforDashboard: {
 		value: function() {
 			Util.waitUntilElementIsVisible(element(by.xpath('//*[@id="dashboard"]/div/div/div[1]/div/div[1]/span')));
 			expect(element(by.xpath('//*[@id="dashboard"]/div/div/div[1]/div/div[1]/span')).getText()).toEqual('Task List');
 			Util.waitUntilElementIsVisible(element(by.xpath('//*[@id="dashboard"]/div/div/div[3]/div/div[1]/span')));
 			expect(element(by.xpath('//*[@id="dashboard"]/div/div/div[3]/div/div[1]/span')).getText()).toEqual('Case List');
 		}
 	},
 	clickonTasksIcon: {
 		value: function() {
 			var EC = protractor.ExpectedConditions;
 			var task = element(by.xpath('/html/body/alfresco-app/div[1]/alfresco-side-menu/div[1]/div[2]/div/div/i'));
 			browser.wait(EC.visibilityOf(task), 5000000);
 			task.click();
 			Util.waitUntilElementIsVisible(element(by.xpath('//*[@id="tasks"]/div/div/div[1]/div[1]')));
 			Util.waitForPage();
 		}
 	},
 	clickonCasesIcon: {
 		value: function() {
 			var EC = protractor.ExpectedConditions;
 			var cases = element(by.xpath('/html/body/alfresco-app/div[1]/alfresco-side-menu/div[1]/div[3]/div/div/i'));
 			browser.wait(EC.visibilityOf(cases), 500000);
 			cases.click();
 			Util.waitUntilElementIsVisible(element(by.xpath('//*[@id="processes"]/div/div/div/div[1]/div[1]')));
 			Util.waitForPage();
 		}
 	},
 	waitForReportsIcon: {
 		value: function() {
 			Util.waitUntilElementIsVisible(element(by.xpath('/html/body/alfresco-app/div[1]/alfresco-side-menu/div[1]/div[4]/div/div/i')));
 		}
 	}
 });