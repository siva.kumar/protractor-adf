var Page = require('astrolabe').Page;
var Util = require('../util/utils');
var navbar = require('../pages/common/navbar');
var TestConfig = require("../test.config");
module.exports = Page.create({
	url: {
		value: TestConfig.admin.loginRoot
	},
	get: {
		value: function() {
			browser.get(this.url);
		}
	},
	navbar: {
		get: function() {
			return navbar;
		}
	},
	render: {
		value: function() {
			Util.waitForPage();
		}
	},
	txtUserName: {
		get: function() {
			return element(by.id('username'));
		}
	},
	txtPassword: {
		get: function() {
			return element(by.css("input[id='password']"));
		}
	},
	waitForElements: {
		value: function() {
			Util.waitUntilElementIsVisible(element(by.id('username')), 60000);
			Util.waitUntilElementIsVisible(element(by.css("input[id='password']")), 60000);
		}
	},
	clearUsername: {
		value: function() {
			Util.waitUntilElementIsVisible(element(by.id('username')));
			this.txtUserName.clear();
		}
	},
	clearPassword: {
		value: function() {
			Util.waitUntilElementIsVisible(element(by.css("input[id='password']")));
			this.txtPassword.clear();
		}
	},
	enterUsername: {
		value: function(name) {
			Util.waitUntilElementIsVisible(element(by.id('username')));
			this.txtUserName.sendKeys(name);
		}
	},
	enterPassword: {
		value: function(password) {
			Util.waitUntilElementIsVisible(element(by.css("input[id='password']")));
			this.txtPassword.clear();
			this.txtPassword.sendKeys(password);
		}
	},
	signInButton: {
		get: function() {
			return element(by.className('center mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored'));
		}
	},
	login: {
		value: function(username, password) {
			this.waitForElements();
			this.enterUsername(username);
			this.enterPassword(password);
			this.clickSignInButton();
			Util.waitForPage();
			// this.waitUntilTheUserIsLogged();
			// Util.waitUntilElementIsVisible(element(by.xpath("html/body/alfresco-app/div[2]/div/img")));
		}
	},
	logout: {
		value: function() {
			var menuButton = element(by.css('menu_drawer'));
			var leftHandMenu = element(by.id('side_menu'));
			var logoutOption = element(by.cssContainingText("a[class='mdl-navigation__link'] > label", "Logout"));
			Util.waitUntilElementIsVisible(menuButton);
			menuButton.click();
			Util.waitUntilElementIsVisible(leftHandMenu);
			Util.waitUntilElementIsVisible(logoutOption);
			logoutOption.click();
			//this.waitForElements();
		}
	},
	clickSignInButton: {
		value: function() {
			Util.waitUntilElementIsVisible(element(by.className('center mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored')));
			this.signInButton.click();
		}
	},
	waitUntilTheUserIsLogged: {
		value: function() {
			var userName = element(by.xpath("html/body/alfresco-app/div[2]/appsmanager-user-profile/div/div/span[1]"));
			Util.waitUntilElementIsVisible(userName);
		}
	},
	LoginFailure: {
		value: function() {
			Util.waitUntilElementIsVisible(element(by.className('error mdl-card__supporting-text')));
		}
	}
});