module.exports = {

  admin: {
      protocol: "http",
      host: "http://119.82.126.250:",
      port: "8089",
      loginRoot: "/alfresco/demo/#/login",
      appstoot: "/#",
      appsManagerContextRoot: "/alfresco/apps/#/appsmanager",
      adminEmail: "admin@app.activiti.com",
      adminPassword: "admin",
      adminFullName: "Muraai Administrator"
  },

  main: {

         protocol: "https",
         host: "localhost",
         seleniumAdress: 'http://localhost:4444/wd/hub',
         port: "9999",
         webContextRoot: "/login",
         apiContextRoot: "/appsmanager",
         adminEmail: "admin",
         adminPassword: "admin",
         adminFullName: "Administrator",
         browser_width: 1600,
         browser_height: 900,
         presence_timeout: 60000
     },
     run: {
          /**
           * List of the suites that are ran by the default grunt tasks.
           * @config suites {array}
           */
          suites: [
              'suites'
          ],
},

};
