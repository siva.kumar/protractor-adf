var exports = module.exports = {};
var EC = protractor.ExpectedConditions;
var isElementPresent = false;
/**
 * Wait for url
 */
exports.waitForPage = function() {
	browser.wait(function() {
		var deferred = protractor.promise.defer();
		browser.executeScript("return document.readyState").then(function(text) {
			deferred.fulfill(function(text) {
				return text === "complete";
			});
		});
		return deferred.promise;
	})
};

exports.waitUntilUrlIsShowed = function(urlToWait, timeout) {
	if (!timeout) {
		timeout = 20000;
	}
	browser.wait(function() {
		return browser.getCurrentUrl().then(function(url) {
			return (url.indexOf(browser.baseUrl + urlToWait.toString()) !== -1);
		}, timeout)
	});
};

exports.waitUntilElementIsVisible = function(elementToCheck, timeout) {
	if (!timeout) {
		timeout = 20000;
	}
	this.waitUntilElementIsPresent(elementToCheck, timeout);
	var isDisplayed = false;
	browser.wait(function() {
		elementToCheck.isDisplayed().then(
			function() {
				isDisplayed = true;
			},
			function(err) {
				isDisplayed = false;
			}
		);
		return isDisplayed;
	}, timeout);
};

exports.waitUntilElementIsPresent = function(elementToCheck, timeout) {
	if (!timeout) {
		timeout = 20000;
	}
	browser.wait(function() {
		elementToCheck.isPresent().then(
			function() {
				isElementPresent = true;
			},
			function(err) {
				isElementPresent = false;
			}
		);
		return isElementPresent;
	}, timeout);
};

exports.generateRandomString = function(length) {
	
    length = typeof length !== 'undefined' ? length : 5;
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	for (var i = 0; i < length; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
};

exports.generateRandomEmail = function(length) {
	
    length = typeof length !== 'undefined' ? length : 5;
	var email = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	for (var i = 0; i < length; i++)
		email += possible.charAt(Math.floor(Math.random() * possible.length));

	email += "@app.activiti.com";
	return email.toLowerCase();
};
exports.generateRandomString = function(length) {

	length = typeof length !== 'undefined' ? length : 5;
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	for (var i = 0; i < length; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
};

exports.generateRandomEmail = function(length) {

	length = typeof length !== 'undefined' ? length : 5;
	var email = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	for (var i = 0; i < length; i++)
		email += possible.charAt(Math.floor(Math.random() * possible.length));

	email += "@app.activiti.com";
	return email.toLowerCase();
};