var Util = require('../../util/utils');
var appsManager = require('../../pages/v2/appsManagerPage');
var appInfoPage = require('../../pages/v2/appsinfoPage');
var appsMappingPage = require('../../pages/v2/appsMappingPage');
var navbar = require('../../pages/common/navbar');
var TestConfig = require("../../test.config");
var edittable = require('../../pages/v2/edittable');
var loginPage = require('../../pages/loginpage');
var sideBar = require('../../pages/v2/sidebarpage');
var appsManagerActivitPage = require('../../pages/v2/appsManagerActiviti');
var EC = protractor.ExpectedConditions;

describe('signInPage Testing', function() {

    beforeEach(function() {
        browser.ignoreSynchronization = true
        browser.get("/alfresco/demo/#/login");
        browser.driver.manage().window().maximize();
    });

    it('1.Should go the appsManagerPage then Get Back to the Home Page', function() {
        loginPage.login(TestConfig.admin.adminEmail, TestConfig.admin.adminPassword);
        sideBar.waitForElement();
        sideBar.clickonSideBar();
        sideBar.clickonAdminPage();
        sideBar.click_on_appsManagerButton();

        appsManagerActivitPage.clickOnClaimApp();
        appsManagerActivitPage.clickOnEdit("Approver", "Clain Managment", "claim.html");
        appsManagerActivitPage.clickOnClaimApp();
        expect(edittable.user_group_row.getText()).toEqual("Approver");
        expect(edittable.adf_page_row.getText()).toEqual("Clain Managment");
        expect(edittable.adf_url_row.getText()).toEqual("claim.html");

        appsManagerActivitPage.clickOnExpenseApp();
        appsManagerActivitPage.clickOnEdit("Manager", "Expense Page Manager", "expense/ExpensepageManager.html");
        appsManagerActivitPage.clickOnExpenseApp();
        expect(edittable.user_group_row.getText()).toEqual("Manager");
        expect(edittable.adf_page_row.getText()).toEqual("Expense Page Manager");
        expect(edittable.adf_url_row.getText()).toEqual("expense/ExpensepageManager.html");


        appsManagerActivitPage.clickOnTravelApp();
        appsManagerActivitPage.clickOnEdit("Admin", "Travel Page Admin", "travel/TravePage.html");
        appsManagerActivitPage.clickOnTravelApp();
        expect(edittable.user_group_row.getText()).toEqual("Admin");
        expect(edittable.adf_page_row.getText()).toEqual("Travel Page Admin");
        expect(edittable.adf_url_row.getText()).toEqual("travel/TravePage.html");

        appsManagerActivitPage.clickOnLeaveApp();
        appsManagerActivitPage.clickOnEdit("Admin", "Leave Page Editing", "leaveEdit/leaveEdit.html");
        appsManagerActivitPage.clickOnLeaveApp();
        expect(edittable.user_group_row.getText()).toEqual("Admin");
        expect(edittable.adf_page_row.getText()).toEqual("Leave Page Editing");
        expect(edittable.adf_url_row.getText()).toEqual("leaveEdit/leaveEdit.html");

    });


});

