var loginpage = require('../../pages/loginpage');
var Util = require('../../util/utils');
var nav = require('../../pages/common/navbar');
var sidemenu = require('../../pages/sideMenu');
var TestConfig = require("../../test.config");

describe('signInPage Testing', function() {
	var SETTINGS = {
		wrongEmail: Util.generateRandomEmail(),
		wrongPassword: Util.generateRandomString(),
		message: "You have entered an invalid username or password"
	};

	beforeEach(function() {
		browser.ignoreSynchronization = true
		loginpage.get();
		browser.driver.manage().window().maximize();

	});
	it('1.Login with wrong username and correct password ', function() {
		loginpage.login(SETTINGS.wrongEmail, TestConfig.admin.adminPassword); //TestConfig.admin.adminEmail, SETTINGS.wrongPassword)
		loginpage.LoginFailure();
		loginpage.clearUsername();
		loginpage.clearPassword();
	});

	it('2.Login with corrcet username and wrong password ', function() {
		loginpage.login(TestConfig.admin.adminEmail, SETTINGS.wrongPassword);
		loginpage.LoginFailure();
		loginpage.clearUsername();
		loginpage.clearPassword();
	});

	it('3.Login with wrong username and apassword ', function() {
		loginpage.login(SETTINGS.wrongEmail, SETTINGS.wrongPassword);
		loginpage.LoginFailure();
		loginpage.clearUsername();
		loginpage.clearPassword();
	});

	it('4.Login with valid username and password', function() {
		loginpage.login(TestConfig.admin.adminEmail, TestConfig.admin.adminPassword);
		nav.LoginSuccess();
	});

	it('5.Login Success and logout ', function() {
		loginpage.login(TestConfig.admin.adminEmail, TestConfig.admin.adminPassword);
		nav.LoginSuccess();
		expect(nav.userinfoname.getText()).toEqual('Muraai Administrator');
		nav.expenseClick();
		sidemenu.waitForDashboardIcon();
		sidemenu.waitforDashboard();
		sidemenu.clickonTasksIcon();
		sidemenu.clickonCasesIcon();
		nav.homePage();
		nav.logOut();
		loginpage.waitForElements();
	});

	// it('6 .Login Success and logout ', function() {
	//        loginpage.login(TestConfig.demo.adminEmail, TestConfig.demo.adminPassword);
	//        nav.LoginSuccess();
	//        nav.appmangerClick();
	//        appsManager.clickclaim();
	//        userappTable.clickonAddButton();

	//       appsmapping.waitForAppMappingElements();
	//       appsmapping.selectOption("Manager");
	//       appsmapping.selectOption("--Select--");
	//       appsmapping.errorselectBox();
	//       expect(appsmapping.geterrorselectBox.getText()).toEqual('Make a proper selection');

	//       appsmapping.enteradfPageName('1');
	//       appsmapping.erroradfPageName();
	//       expect(appsmapping.geterroradfPageName.getText()).toEqual('Page name should start with alphabet, Page name should contain atleast 3 characters');
	//       appsmapping.enteradfpageurl('test');
	//       appsmapping.erroradfpageurl();
	//       expect(appsmapping.geterroradfPageUrl.getText()).toEqual('Page URL should contain webpage extension(html/htm/php/jsp)');
	//       expect(appsmapping.savebutton.isEnabled()).toBe(false);
	//       appsmapping.clickDiscardButton();

	//        nav.homePage();
	//        nav.expenseClick();
	//        nav.logOut();   
	//       loginpage.waitForElements();
	// });

	// it('7 .Login Success and logout ', function() {
	//       loginpage.login(TestConfig.demo.adminEmail, TestConfig.demo.adminPassword);
	//       nav.LoginSuccess();
	//       nav.appmangerClick();
	//       appsManager.clickclaim();

	//       userappTable.cleardefaultadfpage();
	//       userappTable.enterdefaultadfpage('1');
	//       userappTable.waitforerrordefaultpage();
	//       expect(userappTable.errordefaultadfpage.getText()).toEqual('Default directory name should contain 3 characters, Default directory name should start with Alphabet');
	//       expect(userappTable.savebuttonuserexperience.isEnabled()).toBe(false);
	//        nav.homePage();
	//        nav.expenseClick();
	//        nav.logOut();  
	//     //   loginpage.waitForElements();

	// });
});