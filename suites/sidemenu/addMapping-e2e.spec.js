var Util = require('../../util/utils');
var appsManager = require('../../pages/v2/appsManagerPage');
var appInfoPage = require('../../pages/v2/appsinfoPage');
var appsMappingPage = require('../../pages/v2/appsMappingPage');
var navbar = require('../../pages/common/navbar');
var TestConfig = require("../../test.config");
var edittable = require('../../pages/v2/edittable');
var loginPage = require('../../pages/loginpage');
var sideBar = require('../../pages/v2/sidebarpage');
var appsManagerActivitPage = require('../../pages/v2/appsManagerActiviti');
var EC = protractor.ExpectedConditions;


describe('signInPage Testing', function() {

    beforeEach(function() {
        browser.ignoreSynchronization = true
        browser.get("/alfresco/demo/#/login");
        browser.driver.manage().window().maximize();
    });

    
    it('1.Should go the appsManagerPage then Get Back to the Home Page', function() {
        loginPage.login(TestConfig.admin.adminEmail, TestConfig.admin.adminPassword);
        sideBar.waitForElement();
        sideBar.clickonSideBar();
        sideBar.clickonAdminPage();
        sideBar.click_on_appsManagerButton();
        appsManagerActivitPage.clickOnExpenseApp();
        expect(appsManager.appInfo.appName.getText()).toEqual("Expense Management");
        expect(appsManager.appInfo.appId.getText()).toEqual("4");

        appsManagerActivitPage.addAppMapping("Manager", "Expense Page Adding", "expense/Expensepage.html");
        Util.waitForPage();
        appsManagerActivitPage.clickOnExpenseApp();
        appsManagerActivitPage.addAppMapping("Admin", "Expense Page Adding", "expense/Expensepage.html");
        Util.waitForPage();

        appsManagerActivitPage.clickOnClaimApp();
        expect(appsManager.appInfo.appName.getText()).toEqual("Claim Management App");
        expect(appsManager.appInfo.appId.getText()).toEqual("3");
        appsManagerActivitPage.addAppMapping("Approver", "Clain Managment", "claim.html");
        Util.waitForPage();
        appsManagerActivitPage.clickOnClaimApp();
        appsManagerActivitPage.addAppMapping("Manager", "Claim Page Adding", "claim/Claim.html");
        Util.waitForPage();

        appsManagerActivitPage.clickOnLeaveApp();
        expect(appsManager.appInfo.appName.getText()).toEqual("Leave App");
        expect(appsManager.appInfo.appId.getText()).toEqual("1");
        appsManagerActivitPage.addAppMapping("Admin", "Leave Page Edit", "leave/leave.html");
        Util.waitForPage();

        appsManagerActivitPage.clickOnTravelApp();
        expect(appsManager.appInfo.appName.getText()).toEqual("Travel Management");
        expect(appsManager.appInfo.appId.getText()).toEqual("5");
        appsManagerActivitPage.addAppMapping("Manager", "Travel Management ", "Travel.html");
        Util.waitForPage();
        appsManagerActivitPage.clickOnTravelApp();
        appsManagerActivitPage.addAppMapping("Admin", "Travel Page Edit", "travel/travel.html");
        Util.waitForPage();

    });

});

/*
sideBar.waitForElement();
sideBar.clickonSideBar();
sideBar.clickonAdminPage();
sideBar.click_on_appsManagerButton();
sideBar.clickOnApp();
appsManager.waitForAppsManagerElements();
appInfoPage.waitForAppInfoElements();
appsMappingPage.addApps("Admin", "Expense Page Adding", "expen/Travelpage.html");
appsManager.selectAppRandomly();
Util.waitForPage();
*/
