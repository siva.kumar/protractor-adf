var Util = require('../../util/utils');
var appsManager = require('../../pages/v2/appsManagerPage');
var appInfoPage = require('../../pages/v2/appsinfoPage');
var appsMappingPage = require('../../pages/v2/appsMappingPage');
var navbar = require('../../pages/common/navbar');
var TestConfig = require("../../test.config");
var edittable = require('../../pages/v2/edittable');
var loginPage = require('../../pages/loginpage');
var sideBar = require('../../pages/v2/sidebarpage');
var appsManagerActivitPage = require('../../pages/v2/appsManagerActiviti');
var EC = protractor.ExpectedConditions;

describe('signInPage Testing', function() {
    beforeEach(function() {
        browser.ignoreSynchronization = true
        browser.get("/alfresco/demo/#/login");
        browser.driver.manage().window().maximize();
    });

    it('1.Should go the appsManagerPage then Get Back to the Home Page', function() {
        loginPage.login(TestConfig.admin.adminEmail, TestConfig.admin.adminPassword);
        sideBar.waitForElement();
        sideBar.clickonSideBar();
        sideBar.clickonAdminPage();
        sideBar.click_on_appsManagerButton();
        appsManagerActivitPage.clickOnClaimApp();
        edittable.clickon_deletemappingButton();
        appsManagerActivitPage.clickOnExpenseApp();
        edittable.clickon_deletemappingButton();
        appsManagerActivitPage.clickOnTravelApp();
        edittable.clickon_deletemappingButton();
        appsManagerActivitPage.clickOnLeaveApp();
        edittable.clickon_deletemappingButton();
    });
});