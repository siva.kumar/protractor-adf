var loginpage = require('../../pages/loginpage');
var Util = require('../../util/utils');
var nav = require('../../pages/common/navbar');
var sidemenu = require('../../pages/sideMenu');
var appsManagerActivitPage = require('../../pages/v2/appsManagerActiviti');
var dashBoardPage = require('../../pages/v2/dashboardPage');
var TestConfig = require("../../test.config");
describe('signInPage Testing', function() {

	beforeEach(function() {

		browser.ignoreSynchronization = true
		loginpage.get();
		browser.driver.manage().window().maximize();
		loginpage.login(TestConfig.admin.adminEmail, TestConfig.admin.adminPassword);

	});

	it('1.DashBoard Testing', function() {

		nav.LoginSuccess();
		nav.expenseClick();
		appsManagerActivitPage.dashBoardActiviti();
		appsManagerActivitPage.tasksActiviti();
		appsManagerActivitPage.processActivit();
		appsManagerActivitPage.reportActiviti();
		appsManagerActivitPage.activitMenu();
		browser.sleep(2000);
		appsManagerActivitPage.adminActiviti();
		expect(element(by.xpath(".//*[@id='apps_list_search']")).isPresent()).toBeTruthy();

	});

	afterEach(function() {
		appsManagerActivitPage.logOut();
	});

});