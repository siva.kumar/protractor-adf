/*var loginpage = require('../../pages/loginpage');
var Util = require('../../util/utils');
var nav = require('../../pages/common/navbar');
var sidemenu = require('../../pages/sideMenu');
var appsManagerActivitPage = require('../../pages/v2/appsManagerActiviti');
var dashBoardPage = require('../../pages/v2/dashboardPage');
var TestConfig = require("../../test.config");

describe('signInPage Testing', function() {
    beforeEach(function() {
        browser.ignoreSynchronization = true
        loginpage.get();
        browser.driver.manage().window().maximize();
        loginpage.login(TestConfig.admin.adminEmail, TestConfig.admin.adminPassword);
    });

    it('1.TaskMenu Testing ', function() {
        nav.LoginSuccess();
        nav.expenseClick();
        appsManagerActivitPage.activitMenu();
        appsManagerActivitPage.tasksActiviti();
        browser.sleep(2000);
        dashBoardPage.completedTaskOption.click();
        browser.sleep(2000);
        expect(element(by.xpath(".//*[@id='tasks']/div/div/div/activiti-tasklist/div/div/alfresco-datatable/table/thead/tr/th[1]")).isPresent()).toBeTruthy();
        expect(element(by.xpath(".//*[@id='tasks']/div/div/div/activiti-tasklist/div/div/alfresco-datatable/table/thead/tr/th[2]/span")).isPresent()).toBeTruthy();
    });

    it(' PrescessMenu Testing', function() {
        nav.LoginSuccess();
        nav.expenseClick();
        appsManagerActivitPage.activitMenu();
        appsManagerActivitPage.processActivit();
        browser.sleep(2000);
        dashBoardPage.runningProcessOption.click();
        browser.sleep(2000);
        expect(element(by.xpath(".//*[@id='processes']/div/div/div/div[1]/div[1]")).isPresent()).toBeTruthy();
        expect(element(by.xpath(".//*[@id='processes']/div/div/div/div[2]/div")).isPresent()).toBeTruthy();
    });

    it(' Report Menu Testing', function() {
        nav.LoginSuccess();
        nav.expenseClick();
        appsManagerActivitPage.activitMenu();
        appsManagerActivitPage.reportActiviti();
        browser.sleep(2000);
        dashBoardPage.myTaskReportOption.click();
        browser.sleep(2000);
        expect(element(by.xpath("html/body/alfresco-app/div[3]/appsmanager-breadcrumbs/div[1]/div[4]/div")).isPresent()).toBeTruthy();
    });

    it(' Admin menu Testing ', function() {
        nav.LoginSuccess();
        nav.expenseClick();
        appsManagerActivitPage.activitMenu();
        browser.sleep(2000);
        appsManagerActivitPage.adminActiviti();
        expect(element(by.xpath(".//*[@id='apps_list_search']")).isPresent()).toBeTruthy();
    });

    
    it('1.DashBoard Testing', function() {
        nav.LoginSuccess();
        nav.expenseClick();
        appsManagerActivitPage.activitMenu();
        appsManagerActivitPage.dashBoardActiviti();
        expect(element(by.xpath(".//*[@id='dashboard']/div/div/div[1]/div/div[1]")).isPresent()).toBeTruthy();
        appsManagerActivitPage.activitMenu();
        appsManagerActivitPage.tasksActiviti();
        browser.sleep(2000);
        dashBoardPage.completedTaskOption.click();
        expect(element(by.xpath(".//*[@id='tasks']/div/div/div/activiti-tasklist/div/div/alfresco-datatable/table/thead/tr/th[1]")).isPresent()).toBeTruthy();
        expect(element(by.xpath(".//*[@id='tasks']/div/div/div/activiti-tasklist/div/div/alfresco-datatable/table/thead/tr/th[2]/span")).isPresent()).toBeTruthy();
        
        appsManagerActivitPage.activitMenu();
        appsManagerActivitPage.processActivit();
        browser.sleep(2000);
        dashBoardPage.runningProcessOption.click();
        browser.sleep(2000);
        expect(element(by.xpath(".//*[@id='processes']/div/div/div/div[1]/div[1]")).isPresent()).toBeTruthy();
        expect(element(by.xpath(".//*[@id='processes']/div/div/div/div[2]/div")).isPresent()).toBeTruthy();
        
        appsManagerActivitPage.activitMenu();
        appsManagerActivitPage.reportActiviti();
        browser.sleep(2000);
        dashBoardPage.myTaskReportOption.click();
        browser.sleep(2000);
        expect(element(by.xpath("html/body/alfresco-app/div[3]/appsmanager-breadcrumbs/div[1]/div[4]/div")).isPresent()).toBeTruthy();
        
        appsManagerActivitPage.activitMenu();
        browser.sleep(2000);
        appsManagerActivitPage.adminActiviti();
        expect(element(by.xpath(".//*[@id='apps_list_search']")).isPresent()).toBeTruthy();
    });

    afterEach(function() {
        appsManagerActivitPage.logOut();
    });
});*/